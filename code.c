#include <stdio.h>

int retornaValor();
void naoRetornaValor();
void passaValor(int num);
void referencia(int C[], int D[], int igual[]);
void lerMatriz();
void Struct(CALCULO mais);
void recursividade(int n); // uma funcao que chama a si mesma para resolver algo
void arquivoTexto();
typedef struct
{
    int numero;
    int resultado;
} CALCULO; // nome do struct

main(void)
{
    int num = 10, A[5], B[5], result[5];
    CALCULO soma;
    referencia(A, B, result);
    passaValor(num);
    recursividade(num);
    lerMatriz();
    Struct(soma);
    printf("\n%d\n", retornaValor());
    printf("\n%d\n", result[1]);
}

int retornaValor()
{
    int num;
    num = 1 + 1;
    return num;
}

void naoRetornaValor()
{
    int num;
    num = 2 + 2;
    printf("\n%d\n", num);
}

void passaValor(int num)
{
    num = 3 + 3;
    printf("\n%d\n", num);
}

void referencia(int C[5], int D[5], int igual[])
{
    C[1] = 5, D[1] = 6;
    igual = C[1] + D[1];
}

void lerMatriz()
{
    int matriz[3][3];
    for (int i = 0; i < 8; i++)
        for (int j = 0; j < 8; j++)
        {
            {
                matriz[i][j]++;
            }
        }
}

void Struct(CALCULO mais)
{
    int igual;
    mais.numero = 10;
    mais.resultado = 11;
    igual = mais.resultado + mais.numero;
    printf("\n%d\n", igual);
}

void recursividade(int n)
{
    int equal;
    if (n != 0)
    {
        equal = n - 1;
        recursividade(equal);
    }
    printf("%d", equal);
}

void arquivoTexto()
{
    FILE *arquivo;
    arquivo = fopen("texto.txt", "w");
    fclose(arquivo);
}
